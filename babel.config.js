/*global module,process*/
const plugins = [
	'@babel/transform-runtime',
	'@babel/plugin-proposal-class-properties',
];

if (!process.env.JEST_TEST) {
	plugins.push('babel-plugin-istanbul');
}

// eslint-disable-next-line import/no-commonjs
module.exports = {
	'presets': [
		'@babel/preset-env',
	],
	plugins,
};
