import log from '@megatherium/log';

const logger = log.init({
	app: '@megatherium/sleep',
});

export default logger;
