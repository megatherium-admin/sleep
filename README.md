# sleep

Returns a Promise that resolves after a given amount of milliseconds.

See the whole [documentation](https://megatherium.gitlab.io/sleep) or the [coverage report](https://megatherium.gitlab.io/sleep/coverage).

## Getting started

Install the module:

`$ npm install @megatherium/sleep`

Use the module:

```
import sleep from '@megatherium/sleep';

(async() => {
	console.log('Waiting 2 seconds...');
	await sleep(2000);
	console.log('Waited 2 seconds.');
})();
```

## API

### Exports

[sleep](module-sleep.html)`(timeoutInMilliseconds: int): Promise` Returns a Promise that resolves after a given amount of milliseconds.

### Scripts

The following scripts can be executed using `npm run`:
- `build` Builds the module.
- `build-docs` Builds the documentation.
- `build-source` Builds the source code.
- `build-tests` Builds test-cases from jsdoc examples.
- `clear` Clears the module from a previous build.
- `clear-coverage` Clears the coverage reports and caches.
- `clear-docs` Clears the previous documentation build.
- `clear-source` Clears the previous source build.
- `clear-tests` Clears the generated jsdoc example test files.
- `fix` Runs all automated fixes.
- `fix-lint` Automatically fixes linting problems.
- `release` Runs semantic release. Meant to be only executed by the CI, not by human users.
- `test` Executes all tests.
- `test-coverage` Generates coverage reports from the test results using [nyc](https://www.npmjs.com/package/nyc).
- `test-deps` Executes a [depcheck](https://www.npmjs.com/package/depcheck).
- `test-e2e` Executes End-to-End-Tests using [cucumber](https://github.com/cucumber/cucumber-js).
- `test-integration` Executes integration tests using [jest](https://jestjs.io/).
- `test-lint` Executes linting tests using [eslint](https://eslint.org/).
- `test-unit` Executes unit tests using [mocha](https://mochajs.org/).
- `update` Checks for dependency updates using [renovate](https://www.npmjs.com/package/renovate).

## Contribution

See [Contribution Guidelines](CONTRIBUTION.md) for more details.

